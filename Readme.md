## Spectrum.py

spectrum.py is a Python script for extracting the spectrum from a
primal-dual optimal solution to a polynomial matrix program, computed
with [sdpb](https://github.com/davidsd/sdpb).  The script works with
both Python 2 and 3.

## Installation

### Docker

`spectrum.py` is included in the [SDPB docker
image](https://hub.docker.com/r/wlandry/sdpb).  To use the docker
image, you need to map your local directory into a place in the docker
image.  A good default is `/usr/local/share/spectrum`.  Mapping your
current directory onto `/usr/local/share/spectrum` is done with the
`-v` option

    -v $PWD:/usr/local/share/spectrum

An an example, to extract the spectrum using the input file `example.xml` and output
directory `example_out`, the full command line is

    docker run -v $PWD:/usr/local/share/spectrum wlandry/sdpb:2.1.2 spectrum.py -s /usr/local/share/spectrum/example.xml -o /usr/local/share/spectrum/example_out -m /usr/local/share/spectrum/example.spectrum.m -p 1024

which will write the output into `example.spectrum.m`.  Note that the files may be
owned by root.

### Local Install

#### MPSolve

You first need to install the arbitrary precision polynomial
root-finder [MPSolve](http://numpi.dm.unipi.it/mpsolve-2.2/)

You will need to pass the path of the MPSolve executable `unisolve` to
spectrum.py.

#### Python packages

  - [mpmath](http://mpmath.org/) for arbitrary precision arithmetic

  - [lxml](https://lxml.de/) for parsing XML

One way of installing these packages is to use
[pip](https://pypi.python.org/pypi/pip).  After installing pip, follow
these steps.

  - Install virtualenv:

        $ pip install virtualenv

  - Create a new virtual environment for spectrum.py:

        $ cd spectrum-extraction
        $ virtualenv venv

  - Activate the virtual environment:

        $ source venv/bin/activate

  - Install the requirements:

        $ pip install -r requirements.txt

  - Hopefully that works? You'll have to do `source venv/bin/activate'
    before using spectrum.py in the future.

## Attribution

This script was originally written for use in:

  - Komargodski, Zohar and Simmons-Duffin, David, 'The Random Bond
    Ising Model in 2.01 and 3 Dimensions,' [arXiv:1603.04444](https://arxiv.org/abs/1603.04444)

An explanation of how it works appears in:

  - Simmons-Duffin, David, 'The Lightcone Bootstrap and the Spectrum of the 3d Ising CFT,' [arXiv:1612.08471](https://arxiv.org/abs/1612.08471)

Author(s): David Simmons-Duffin,...

